import React from 'react';
import ReactDOM from 'react-dom';
import Basic from './App.jsx';

ReactDOM.render(<Basic />, document.getElementById('basic'));