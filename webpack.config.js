var config = {
    mode: "development",
    entry: './main.js',
    output: {
        path: __dirname + "/dist",
        filename: 'main.js',
    },
    devServer: {
        inline: true,
        port: 8080
    },
    module: {        
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                query: {
                    presets: ['es2015', 'react']
                }
            }
        ]
    }
}
module.exports = config;

